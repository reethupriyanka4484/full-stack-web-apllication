from flask import Flask, request, render_template, send_from_directory
from flask import jsonify
import json
import requests
__author__ = 'reethu'

app = Flask(__name__)

@app.route("/")
def RegisterPage():
    return render_template('Register.html')
@app.route("/login")
def login():
    return render_template('login.html')
if __name__ == "__main__":
    app.run()
